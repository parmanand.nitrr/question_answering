import pandas as pd
import spacy
import time
from spacy import displacy
import numpy as np
from spacy.gold import biluo_tags_from_offsets

nlp = spacy.load("en_core_web_sm")


def char_offset_to_token_offset(paragraph, start, end):
    """
    TODO: write edge cases
    :param paragraph:
    :param start:
    :param end:
    :return:
    """
    start_time = time.time()
    start_idx = 0
    end_idx = 0
    start_char_idx = 0
    end_char_idx = 0
    start_flag = False
    end_flag = False
    token_idx = 0
    char_idx = 0
    span = paragraph[start:end]
    for i in paragraph.split():
        if not start_flag:
            if char_idx == start:
                start_flag = True
                start_idx = token_idx
                end_idx = start_idx + len(span.split()) - 1
                start_char_idx = char_idx
                end_char_idx = start_char_idx + len(span)

        token_idx += 1
        char_idx += 1 + len(i)  # space

    end_idx = end_idx + 1
    result_span = " ".join(paragraph.split(" ")[start_idx:end_idx])
    span_tokens = span.split()
    res_span_tokens = result_span.split()
    print(span_tokens, "|", span_tokens)
    print(start, end, start_idx, end_idx, start_char_idx, end_char_idx)
    if len(span_tokens) != len(res_span_tokens):
        print(paragraph.split())
    assert len(span_tokens) == len(res_span_tokens)

    print("--- %s seconds ---" % (time.time() - start_time))
    return start_idx, end_idx



def char_offset_to_token_offset_df(data_df, outpath):
    counter = 0
    for row in data_df.iterrows():
        index = row[0]
        paragraph = row[1][1]
        span = row[1][2]
        start = row[1][3]
        end = row[1][4]
        # span = paragraph[start:end]

        doc = nlp(paragraph)

        entities = [(start, end, "ANSWER")]

        tags = biluo_tags_from_offsets(doc, entities)

        try:
            if "U-ANSWER" in tags:
                start_tok_idx = tags.index('U-ANSWER')
                end_tok_idx = start_tok_idx
            elif "B-ANSWER" in tags:
                start_tok_idx = tags.index('B-ANSWER')
                end_tok_idx = tags.index('L-ANSWER')
            else:
                #         start_tok_idx = tags.index('-')
                #         end_tok_idx = start_tok_idx + len(span.split())-1
                # #         print(start_tok_idx,end_tok_idx, doc[start_tok_idx:end_tok_idx+1])
                continue
            data_df.iloc[index, data_df.columns.get_loc('start_token')] = start_tok_idx
            data_df.iloc[index, data_df.columns.get_loc('end_token')] = end_tok_idx
            counter += 1

            result_span = doc[start_tok_idx:end_tok_idx + 1]
            assert span == str(result_span)
        except Exception as AssertionError:
            continue



    data_df.to_csv(outpath,index=False)

if __name__ == '__main__':
    train_df = pd.read_csv("D:\\SQUAD2\\train.csv", header=None,
                           names=["question","paragraph","answer_span","start","end",])


    train_df["q_count"] = train_df["question"].map(lambda question: len(question.split()))
    train_df["p_count"] = train_df["paragraph"].map(lambda question: len(question.split()))

    train_df["start_token"] = np.NAN
    train_df["end_token"] = np.NAN

    dev_df = pd.read_csv("D:\\SQUAD2\\dev.csv", header=None,
                         names=["question", "paragraph", "answer_span", "start", "end", ])

    dev_df["q_count"] = train_df["question"].map(lambda question: len(question.split()))
    dev_df["p_count"] = train_df["paragraph"].map(lambda question: len(question.split()))

    dev_df["start_token"] = np.NAN
    dev_df["end_token"] = np.NAN


    char_offset_to_token_offset_df(train_df,"D:\\SQUAD2\\processed_train.csv")
    char_offset_to_token_offset_df(dev_df,"D:\\SQUAD2\\processed_dev.csv")


