import torch
from torch.utils.data import Dataset, DataLoader
from torchtext import data, vocab
from torchtext.data import get_tokenizer
import torch.nn as nn
import torch.nn.functional as F
import torch
import time
import torch.optim as optim
from test2 import TEXT,train_iter, val_iter

class BilinearSeqAttn(nn.Module):
    """
    A bilinear attention layer over a sequence X w.r.t y:
    * o_i = softmax(x_i'Wy) for x_i in X.
    """
    def __init__(self, x_size, y_size,normalize=True):
        super(BilinearSeqAttn, self).__init__()
        self.linear = nn.Linear(y_size, x_size)
        self.normalize = normalize

    def forward(self, x, y, x_mask):
        Wy = self.linear(y)
        xWy = torch.bmm(Wy.unsqueeze(1),x).squeeze(1)
        # xWy.data.masked_fill_(1-x_mask.data, -float('inf'))
        xWy[~x_mask]= float('-inf')
        # if self.training:
        #     # In training we output log-softmax for NLL
        #     alpha = F.log_softmax(xWy, dim=-1)
        # else:
            # ...Otherwise 0-1 probabilities
        alpha = F.softmax(xWy, dim=-1)

        return alpha


class question_answering_model(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_size, num_layers,max_len=200,padding_idx=1):
        super(question_answering_model,self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim,padding_idx =padding_idx)

        self.context_rnn = nn.GRU(input_size=embedding_dim,
                                  hidden_size=hidden_size,
                                  num_layers=num_layers,
                                  bidirectional=True,batch_first=True)

        self.ques_rnn = nn.GRU(input_size=embedding_dim,
                               hidden_size=hidden_size,
                               num_layers=num_layers,
                               bidirectional=True,batch_first=True)

        # Output sizes of rnn encoders
        doc_hidden_size = 2 * hidden_size
        question_hidden_size = 2 * hidden_size

        # Bilinear attention for span start/end
        self.start_attn = BilinearSeqAttn(
            doc_hidden_size,
            question_hidden_size,
        )

        self.end_attn = BilinearSeqAttn(
            doc_hidden_size,
            question_hidden_size,
        )


    def forward(self, context_idxs, question_idxs,c_mask,q_mask):

        # c_mask =  c_mask.permute(1,0)

        c_len, q_len = c_mask.sum(-1), q_mask.sum(-1)

        context_emb = self.embedding(context_idxs)
        question_emb = self.embedding(question_idxs)

        # Encode document with RNN
        c_output, _ = self.context_rnn(context_emb)
        c_output =  c_output.permute(0, 2, 1)
        # Encode question with RNN
        _, q_hidden = self.ques_rnn(question_emb)
        # permute to (batch_size, num_rnn, feature_size)
        q_hidden =  q_hidden.permute(1, 0, 2)
        q_hidden =  q_hidden.contiguous().view(q_hidden.size(0), -1)

        # Predict start and end positions
        start_scores = self.start_attn(c_output, q_hidden,c_mask)
        end_scores = self.end_attn(c_output, q_hidden,c_mask)
        return start_scores, end_scores


def train_model(model,loss_function, optimizer, data_loader):
    # set model to training mode
    model.train()

    current_loss = 0.0
    current_acc = 0

    # iterate over the training data
    for ( question_idxs,context_idxs), (y1, y2) in data_loader:

        # send the input/labels to the GPU
        context_idxs = context_idxs.to(device)
        question_idxs = question_idxs.to(device)
        y1 = y1.to(device)
        y2 = y2.to(device)
        # print(y1)
        c_mask = torch.ones_like(context_idxs) != context_idxs
        batch_size = context_idxs.size(0)
        q_mask = torch.ones_like(question_idxs) != question_idxs


        # zero the parameter gradients
        optimizer.zero_grad()

        with torch.set_grad_enabled(True):
            # forward
            score_s, score_e = model(context_idxs,question_idxs,c_mask,q_mask)
            _, prediction1 = torch.max(score_s, 1)
            # print("prediction",prediction1)
            _, prediction2 = torch.max(score_e, 1)
            loss = loss_function(score_s, y1) + loss_function(score_e, y2)

            #backward
            loss.backward()
            optimizer.step()

        # statistics
        current_loss += loss.item() * context_idxs.size(0)
        current_acc += torch.sum(prediction1 == y1)
        current_acc += torch.sum(prediction2 == y2)


    total_loss = current_loss / len(data_loader.dataset)
    total_acc = current_acc / len(data_loader.dataset)

    print('Train Loss: {:.4f}; Accuracy: {:.4f}'.format(total_loss,total_acc))
    return total_loss, total_acc


def validation(model, data_loader):
    # set model in evaluation mode
    model.eval()

    current_loss = 0.0
    current_acc = 0

    # iterate over  the validation data
    for batch_idx, batch in enumerate(data_loader):
        # send the input/labels to the GPU
        text, target = batch.text, batch.label
        if is_cuda:
            text, target = text.cuda(), target.cuda()

        # forward
        with torch.set_grad_enabled(False):
            score_s, score_e = model(inputs)
            _, predictions = torch.max(outputs, 1)
            loss = loss_function(outputs, labels)

        # statistics
        current_loss += loss.item() * inputs.size(0)
        current_acc += torch.sum(predictions == labels.data)

    total_loss = current_loss / len(data_loader.dataset)
    total_acc = current_acc.double() / len(data_loader.dataset)

    print('Test Loss: {:.4f}; Accuracy: {:.4f}'.format(total_loss,total_acc))

    return total_loss, total_acc


if __name__ == '__main__':

    model = question_answering_model(vocab_size=10000,embedding_dim=100,hidden_size=128,num_layers=1)
    model.embedding.weight.data = TEXT.vocab.vectors
    start_time = time.time()

    # select gpu 0, if available# otherwise fallback to cpu
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    loss_function = nn.CrossEntropyLoss()
    # transfer the model to the GPU
    model = model.to(device)
    # We'll optimize all parameters
    optimizer = optim.Adam(model.parameters())

    EPOCHS = 1000
    for epoch in range(EPOCHS):
        print('Epoch {}/{}'.format(epoch + 1, EPOCHS))

        train_loss,train_accuracy = train_model(model, loss_function,optimizer, train_iter)
        # test_loss, test_accuracy = validation(model, val_iter)
    endtime = time.time() - start_time
    print("Endtime %s seconds",endtime)



