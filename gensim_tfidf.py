import spacy
from spacy import displacy
from gensim import corpora
from gensim import models
import numpy as np
from gensim import similarities

if __name__ == '__main__':


    nlp = spacy.load("en_core_web_lg")

    sentences = ['N-say) (born September 4, 1981) is an American singer, songwriter, record producer and actress.',
     "Born and raised in Houston, Texas, she performed in various singing and dancing competitions as a child, and rose to fame in the late 1990s as lead singer of R&B girl-group Destiny's Child.",
     "Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups of all time.",
     'Their hiatus saw the release of Beyoncé\'s debut album, Dangerously in Love (2003), which established her as a solo artist worldwide, earned five Grammy Awards and featured the Billboard Hot 100 number-one singles "Crazy in Love" and "Baby Boy".']

    ques = "When did Beyonce start becoming popular?"

    q_doc = nlp(ques)

    ques_tokens = [token.text for token in q_doc]

    context_tokens = []

    for i in sentences:
        c_doc = nlp(i)
        context_tokens.append([token.text for token in c_doc])

    processed_corpus = [ques_tokens]+context_tokens
    dictionary = corpora.Dictionary(processed_corpus)  #create dictionary

    bow_corpus = [dictionary.doc2bow(text) for text in processed_corpus]

    # train the model
    tfidf = models.TfidfModel(bow_corpus)
    tfidf[bow_corpus]


    sim_index = similarities.SparseMatrixSimilarity(tfidf[bow_corpus], num_features=12)

    query_document = ques.split()
    query_bow = dictionary.doc2bow(query_document)

