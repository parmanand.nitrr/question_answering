import pandas as pd
import spacy
import time
from spacy import displacy
import numpy as np
from spacy.gold import biluo_tags_from_offsets
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize
from spacy.pipeline import EntityRuler
import json




def train_squad_to_json(filepath, record_path):
    with open(filepath, "r") as file:
        data = json.load(file)
        df1 = json_normalize(data, record_path)
        df2 = json_normalize(data, record_path[:-1])
        df3 = json_normalize(data, record_path[:-2])
        # print(df1.head())
        repeated_context = np.repeat(df3['context'].values, df3.qas.str.len())  # repeat for each question
        df2['context'] = repeated_context
        repeated_ids = np.repeat(df2['id'].values, df2['answers'].str.len())  # repeat for each answer
        # repeated_ids = np.repeat(df2['id'].values, df2['answers'].str.len())  # repeat for each answer
        df1['q_idx'] = repeated_ids
        result_df = df2[['id', 'question', 'context', 'answers', ]].set_index('id').reset_index()
        result_df['context_id'] = result_df['context'].factorize()[0]
        result_df['answer_span'] = result_df['answers'].map(
            lambda answers: answers[0]['text'] if len(answers) > 0 else answers)

        result_df['answer_start'] = result_df['answers'].map(
            lambda answers: answers[0]['answer_start'] if len(answers) > 0 else answers)

        result_df['answer_end'] = result_df['answers'].map(
            lambda answers: answers[0]['answer_start'] + len(answers[0]['text']) if len(answers) > 0 else answers)
        result_df['length'] = result_df['answers'].map(lambda answers: len(answers))
        result_df.drop(columns=["answers", 'length'], inplace=True)
        result_df["answer_start"] = result_df["answer_start"].map(
            lambda x: -1 if type(x) is list else x)  # unknown answer
        result_df["answer_end"] = result_df["answer_end"].map(lambda x: -1 if type(x) is list else x)
        result_df = result_df[result_df.answer_start != -1]
        return result_df


def dev_squad_to_json(filepath, record_path):
    with open(filepath, "r") as file:
        data = json.load(file)
        df1 = json_normalize(data, record_path)
        df2 = json_normalize(data, record_path[:-1])
        df3 = json_normalize(data, record_path[:-2])
        repeated_context = np.repeat(df3['context'].values, df3.qas.str.len())  # repeat for each question
        df2['context'] = repeated_context
        repeated_ids = np.repeat(df2['id'].values, df2['answers'].str.len())  # repeat for each answer
        # repeated_ids = np.repeat(df2['id'].values,1)  # repeat for each answer
        df1['q_idx'] = repeated_ids
        result_df = df2[['id', 'question', 'context', 'answers']].set_index('id').reset_index()
        result_df['context_id'] = result_df['context'].factorize()[0]

        result_df['answer_span'] = result_df['answers'].map(
            lambda answers: answers[0]['text'] if len(answers) > 0 else answers)
        result_df['answer_start'] = result_df['answers'].map(
            lambda answers: answers[0]['answer_start'] if len(answers) > 0 else answers)

        result_df['answer_end'] = result_df['answers'].map(
            lambda answers: answers[0]['answer_start'] + len(answers[0]['text']) if len(answers) > 0 else answers)

        result_df['length'] = result_df['answers'].map(lambda answers: len(answers))

        result_df.drop(columns=["answers", "length"], inplace=True)
        result_df["answer_start"] = result_df["answer_start"].map(lambda x: -1 if type(x) is list else x)
        result_df["answer_end"] = result_df["answer_end"].map(lambda x: -1 if type(x) is list else x)
        result_df = result_df[result_df.answer_start != -1]

        return result_df

def test_squad_to_json(filepath, record_path):
    with open(filepath, "r") as file:
        data = json.load(file)
        df1 = json_normalize(data, record_path)
        df2 = json_normalize(data, record_path[:-1])
        df3 = json_normalize(data, record_path[:-2])
        repeated_context = np.repeat(df3['context'].values, df3.qas.str.len())  # repeat for each question
        df2['context'] = repeated_context
        # repeated_ids = np.repeat(df2['id'].values, df2['answers'].str.len())  # repeat for each answer
        # df1['q_idx'] = repeated_ids
        result_df = df2[['id', 'question', 'context']].set_index('id').reset_index()
        result_df['context_id'] = result_df['context'].factorize()[0]

        return result_df

def char_offset_to_token_offset_df(data_df, outpath):
    print("Started Converting Char offset to token offset format")

    data_df["start_token"] = np.NAN
    data_df["end_token"] = np.NAN
    data_df["spacy_span"] = np.NAN
    index = 0
    for row in data_df.iterrows():
        nlp = spacy.load("en_core_web_sm")
        disabled = nlp.disable_pipes(["ner"])
        ruler = EntityRuler(nlp)
        paragraph = row[1][2]
        span = row[1][4]
        print(index,span,data_df.columns.get_loc('start_token'),data_df.columns.get_loc('end_token'),data_df.columns.get_loc('spacy_span'))

        patterns = [{"label": "ANSWER", "pattern":span}]
        ruler.add_patterns(patterns)
        nlp.add_pipe(ruler)

        para_doc = nlp(paragraph)

        for (idx,k) in enumerate(para_doc):
            print(idx, k.text)
        for ent in para_doc.ents:
            data_df.iloc[index, data_df.columns.get_loc('start_token')] = ent.start
            data_df.iloc[index, data_df.columns.get_loc('end_token')] =  ent.start
            data_df.iloc[index, data_df.columns.get_loc('spacy_span')] = ent.text
        index = index+1
        # result_span = doc[start_tok_idx:end_tok_idx + 1]
        # assert span == str(result_span)
        # except Exception as AssertionError:
        #     continue

    print("Shape of after conversion",data_df.shape)
    data_df.to_csv(outpath,index=False)

if __name__ == '__main__':
    TRAIN_PATH = "D:\\SQUAD2\\train-v2.0.json"
    DEV_PATH = "D:\\SQUAD2\\dev-v2.0.json"
    TEST_PATH = "D:\\SQUAD2\\test-v2.0.json"

    record_path = ['data', 'paragraphs', 'qas', 'answers']
    train_df = train_squad_to_json(TRAIN_PATH, record_path)


    print("Training Shape", train_df.shape)
    train_df.to_csv("D:\\SQUAD2\\train.csv", sep=",", index=False)
    # train_df.to_json("./data/train.json",orient='split')

    dev_df = dev_squad_to_json(DEV_PATH, record_path)
    print("Dev Shape", dev_df.shape)
    dev_df.to_csv("D:\\SQUAD2\\dev.csv", sep=",", index=False,)
    # dev_df.to_json("./data/dev.json",orient='split')

    #
    # test_df = test_squad_to_json(TEST_PATH, record_path)
    # print("test Shape", test_df.shape)
    # test_df.to_csv("D:\\SQUAD2\\test.csv", sep=",", index=False)
    #
    # print(train_df.columns.values)
    # print(dev_df.columns.values)
    dev_df = dev_df[:10]


    # char_offset_to_token_offset_df(train_df,"D:\\SQUAD2\\processed_train.csv")
    char_offset_to_token_offset_df(dev_df,"D:\\SQUAD2\\processed_dev.csv")

