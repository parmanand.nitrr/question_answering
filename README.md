- Download SQuAD 2.0 dataset from https://rajpurkar.github.io/SQuAD-explorer/

- Place the downloaded .json dataset in the `data` directory

- Run `python json_to_csv.py` to generate base dataset for Deep Learning and Machine Learning approaches

- Run `python preprocess.py` to generate processed dataset for Deep Learning and Machine Learning approaches

- Run `python train_dl.py` to run Deep Learning model on the processed(for DL) dataset

- Run `python train_ml.py` to run Machine Learning models on the processed(for ML) dataset

- Extra: Run `jupyter notebook` and open squad_analysis.ipynb on jupyter notebook to see the visualization of the data analysis