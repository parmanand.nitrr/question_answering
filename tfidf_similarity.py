import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel,cosine_similarity,euclidean_distances,laplacian_kernel


tf = TfidfVectorizer(analyzer='word', ngram_range=(1,1), min_df = 1, stop_words = 'english')

sentences = ['N-say) (born September 4, 1981) is an American singer, songwriter, record producer and actress.',
             "Born and raised in Houston, Texas, she performed in various singing and dancing competitions as a child, and rose to fame in the late 1990s as lead singer of R&B girl-group Destiny's Child.",
             "Managed by her father, Mathew Knowles, the group became one of the world's best-selling girl groups of all time.",
             'Their hiatus saw the release of Beyoncé\'s debut album, Dangerously in Love (2003), which established her as a solo artist worldwide, earned five Grammy Awards and featured the Billboard Hot 100 number-one singles "Crazy in Love" and "Baby Boy".']

question = ["When did Beyonce start becoming popular?"]


def get_tfidf(question,sentences,sim_func):
    index = 0
    documents = question + sentences
    tfidf_matrix = tf.fit_transform(documents)
    sim_func = laplacian_kernel
    sims = sim_func(tfidf_matrix[index], tfidf_matrix[index+1:]).flatten()
    return np.argmax(sims)

max_index = get_tfidf(question,question,cosine_similarity)
print(max_index)