import torch
from torch.utils.data import Dataset, DataLoader
from torchtext import data, vocab
from torchtext.data import get_tokenizer
import torch.nn as nn
import torch.nn.functional as F
import torch
import spacy

TEXT = data.Field(use_vocab=True, fix_length=100, batch_first=True, lower=True, tokenize="spacy")
TARGET = data.Field(sequential=False, use_vocab=False, is_target=True, batch_first=True)

FIELD = [('question', TEXT),
         ('paragraph', TEXT),
         ('start_token', TARGET),
         ('end_token', TARGET)
         ]

train, val = data.TabularDataset.splits(path='D:\\SQUAD2\\', train='processed_train_nn.csv',
                                        validation='processed_dev_nn.csv', format='csv',
                                        fields=FIELD)

TEXT.build_vocab(train, vectors=vocab.GloVe(name='6B', dim=300))
TEXT.build_vocab(val, vectors=vocab.GloVe(name='6B', dim=300))

train_iter, val_iter = data.BucketIterator.splits((train, val), batch_sizes=(128, 128), shuffle=True,sort_key=lambda x: len(x.paragraph))

if __name__ == '__main__':
    for (q, c), (y1, y2) in train_iter:
        print(q.shape,c.shape,y1.shape, y2.shape)

